from django.urls import path
from projects.views import project_one, project_list, create_project

urlpatterns = [
    path("", project_one, name="list_projects"),
    path("<int:id>/", project_list, name="show_project"),
    path("create/", create_project, name="create_project"),
]
